title: Syllabus: A Collective Writing Retreat, Rijeka, November 25-29, 2019
---
body:

>*The syllabus is the manifesto of the twenty-first century.* - HyperReadings.info

#**Pirate.Care : A Collective Syllabus Writing Retreat**
*25-29 November, 2019*

**Public round table discussion: 28 November, 2019, 7-9pm**
Filodrammatica (large hall), Korzo 28/1, Rijeka

Participants: Laura Benítez Valero｜Emina Bužinkić ｜ Rasmus Fleischer｜ Maddalena Fragnito ｜Valeria Graziano ｜ Mary Maggic ｜Iva Marčetić  ｜Marcell Mars ｜Tomislav Medak ｜Power Makes Us Sick (PMS) ｜Zoe Romano ｜Ivory Tuesday ｜Ana Vilenica

The project is focused on bottom-up responses to the current ‘care crisis’ that experiment with alternative forms of self-organisation, tools and technologies.  

After a few reading group meetings at Youth Cultural Centre Palach, from 25th to 30th November 2019, Pirate.Care will hold a writing retreat for an international cohort of participants who will create a collaborative online syllabus to document pirate care practices and build resources for political pedagogy from these practices.
 
The writing of a collective syllabus was inspired by and greatly indebted to the wealth of crowdsourced hashtag syllabi generated in recent years by social justice movements fighting racial, environmental and gender inequalities and the long standing genealogy of radical political pedagogies in the history of struggles and organising.
 
The syllabus will be developed on an online publishing platform allowing collaborative writing, remixing and maintaining a catalog of materials, resources and references.
 
The participants of the syllabus writing retreat will be sharing their work in progress and presenting their practices during a public round table. The discussion will be held at Filodrammatica, on Thursday, 28 November, at 7 pm. The event is free and open to all.

In 2020, the pirate care syllabus will be activated through an exhibition and summer camp open to the public as part of the Rijeka 2020 – European Capital of Culture’s programme Dopolavoro, a series of cultural and artistic events imagining the social changes brought about by the end of full employment as a viable horizon for the post-industrial era.

Convened by Valeria Graziano, Marcell Mars and Tomislav Medak (Centre for Postdigital Cultures, Coventry University), Pirate.Care is a research project commissioned by [Drugo More](http://drugo-more.hr/en/0, as part of the flagship programme [Dopolavoro](https://rijeka2020.eu/en/dopolavoro/) for [Rijeka 2020 – European Capital of Culture](https://rijeka2020.eu/en/).


*Accessibility: Please contact Drugo More for details on accessibility and other support needs during your visit:  t./f. +385 51 212 957 ｜ Davor Mišković / davor@drugo-more.hr ｜Ivana Katić / ivana@drugo-more.hr*





##**Contributors’ Biographies:**

###Laura Benítez Valero
holds a PhD in Philosophy, is a researcher and independent curator. Her research connects philosophy, art(s) and techno-science. Currently, her work revolves around the practices of bioart, biohacking, bio-resistance processes, civil biodisobedience, and non-human agents. She is a professor of Critical and Cultural Studies at La Massana (Art and Design Centre) and an external professor of Technology at Elisava. She has worked as a coordinator at the Institute of Humanities of Barcelona/CCCB. She has been a guest researcher at the Ars Electronica Centre and the MACBA documentation center. She has also been invited to different international institutions such as Interface Cultures Kunstuniersität Linz, Sónar Festival (Bcn/Hong Kong), Royal Academy of Arts London or University of Puerto Rico. She currently collaborates in different research projects, both academic and autonomous, and is the director of Biofriction, a European project led by Hangar in collaboration with Bioart Society, Kersnikova Institut, and Cultivamos Cultura. (https://laurabenitezvalero.com/).

###Emina Bužinkić
is a political activist exploring intersections of migration, education, racism, solidarity, and social imagination. Her work revolves around the critical understanding of inscriptions of border regimes in schooling and everyday life, primarily through the criminalization governmentality. She is currently obtaining her PhD in critical studies in education and human rights at the University of Minnesota in the United States. Her work in Croatia is with the [Centre for Peace Studies](https://www.cms.hr/en) [Centar za mirovne studije], the Welcome Initiative [Inicijativa Dobrodošli] and the Taste of Home [Okus doma].

###Rasmus Fleischer
is a historian and essayist, working at Stockholm University. His academic work has mostly concerned media history and political economy, with a particular focus on the commodification of music. Most recently, he has co-authored the book Spotify Teardown (MIT Press, 2019). This year he is initiating a new research project about how economic statistics is dealing with notions of qualitative change, a topic he also touched on in a talk a last year's Transmediale. He was also a member of The Bureau for Piracy (2003–2009). (https://www.rasmusfleischer.se/)

###Maddalena Fragnito
doctoral student at the Centre for Postdigital Cultures, Coventry University, is a cultural activist exploring the intersections between art, transfeminism, critical theory, technology and political activism, and focusing on practices of commoning social reproduction and reappropriation of time, pleasure and spaces. She cofounded  [MACAO](www.macaomilano.org) (2012), an autonomous cultural centre in Milan, [SopraSotto](http://soprasottomilano.it/) (2013), a self-managed kindergarten, and [Landscape Choreography](http://www.landcho.eu/) (2012), an art collective focusing on the direct participation of bodies in the construction of transformational physical and relational landscapes. (https://www.maddalenafragnito.com/)

###Valeria Graziano
works as a research fellow at the [Centre for Postdigital Cultures](https://www.coventry.ac.uk/research/areas-of-research/postdigital-cultures/), Coventry University and she is a co-convenor of Pirate.Care. For many years, she has been involved in numerous initiatives of militant research and collective pedagogy across art institutions and social movements. Her research looks at the organization of practices and tecnopolitical tools that foster the refusal of work and the possibility of political pleasure. She is a founding member of the [Postoffice research group](https://postoffice.media/) and of the Network for Institutional Analysis (UK). Some of her recent publications include: ‘Repair Matters’, a special issue of ephemera: theory & politics in organization (May 2019); ‘Learning from #Syllabus’ (in State Machines, Institute of Network Cultures, 2019); ‘Recreation at Stake’ (in Live Gathering, b_books, 2019). (https://hcommons.org/members/valerix/)

###Mary Maggic
is a non-binary artist working at the intersection of biotechnology and cultural discourse. Their work spans amateur science, public workshopology, participatory performance, documentary, and speculative fiction. Maggic's most recent projects Open Source Estrogen and Estrofem! Lab generate DIY protocols for the extraction and detection of estrogen hormone from bodies and environments, demonstrating its micro-performativity and potential for mutagenesis (i.e. gender-hacking) and toxicological embrace. They hold a BSA in Biological Science and Art from Carnegie Mellon University and a MS in Media Arts and Sciences from MIT Media Lab, and their work has been featured at several festivals and international venues including Haus der Kulturen der Welt (Transmediale; Berlin), Never Apart (Sight+Sound Festival; Montreal), Haus der elektronischen Kunst (Basel), Jeu de Paume (Paris), Institute of Contemporary Arts (Post-Cyberfeminist International; London), out sight (Seoul), Spring Workshop (Hong Kong) and CKSTER Gender Hacking Festival (Berne). Maggic is a recipient of the Prix Ars Electronica Honorary Mention in Hybrid Arts (2017) and a 10-month Fulbright residency in Yogyakarta (2019). (http://www.maggic.ooo/)

###Iva Marčetić
was born in Banja Luka, Bosnia and Herzegovina in 1982. She holds a Master degree in Architecture and urban planning from the school of Architecture, University of Zagreb. As an architect she is interested in building networks between activists, planners and grassroots initiatives in an effort to democratize the process of planning and shift the narrative produced in schools, institutions and practice of architecture and planning that lead to commercialization of public space and infrastructure and gentrification of cities. For years she has also been researching and working on the right to housing issues as a researcher and organizer. She is the one of the coordinators of Housing for all European citizen initiative and a member of European coalition for right to housing and the city. She is one of the founding members of cooperative for architectural practice and research Open Architecture. For the last fifteen years she has been a part of the activist collectives and movements fighting against privatization of public goods and resources both in Croatia, but also in other countries of ex Yugoslavia. She was part of the collective Pulska grupa that represented Croatia in the 13th architectural Venice biennale and a fellow of Akademie Schloss Solitude in Stuttgart. Iva works in Right to the city, Zagreb as a community organizer and researcher for housing and urban planning. She lives in Zagreb, Croatia.

###Marcell Mars
is a research fellow at the Centre for Postdigital Cultures, Coventry University. Mars is one of the founders of [Multimedia Institute/MAMA](https://mi2.hr/en/) in Zagreb. His research Ruling Class Studies, started at the Jan van Eyck Academy (2011), examines state-of-the-art digital innovation, adaptation, and intelligence created by corporations such as Google, Amazon, Facebook, and eBay. He is a doctoral student at Digital Cultures Research Lab at Leuphana University, writing a thesis on Foreshadowed Libraries. Together with Tomislav Medak he founded [Memory of the World/Public Library](https://library.memoryoftheworld.org/#/books/), for which he develops and maintains software infrastructure. Mars is one of the convenors of Pirate.Care.

###Tomislav Medak
is a doctoral student at the Centre for Postdigital Cultures, Coventry University. Medak is a member of the theory and publishing team of the [Multimedia Institute/MAMA](https://mi2.hr/en/) in Zagreb, as well as an amateur librarian for the [Memory of the World/Public Library](https://library.memoryoftheworld.org/#/books/) project. His research focuses on technologies, capitalist development, and postcapitalist transition, particularly on economies of intellectual property and unevenness of technoscience. He authored two short volumes: The Hard Matter of Abstraction—A Guidebook to Domination by Abstraction and Shit Tech for A Shitty World. Together with Marcell Mars he co-edited ‘Public Library’ and ‘Guerrilla Open Access’. Medak is one of the convenors of Pirate.Care. (http://tom.medak.click/en/)

###Power Makes Us Sick (PMS)
is a creative research project focusing on autonomous health care practices and networks from a feminist perspective. PMS seeks to understand the ways that our mental, physical, and social health is impacted by imbalances in and abuses of power. We understand that mobility, forced or otherwise, is an increasingly common aspect of life in the anthropocene. In this quest for placeless solidarity, we start with health. PMS is motivated to develop free tools of solidarity, resistance, and sabotage that are informed by a deep concern for planetary well-being. (https://pms.hotglue.me/) 

###Zoe Romano
is a Milan-based activist, who since 1998 has been directly involved in co-developing several projects in the context of the Italian and European post-1999 movement, such as Chainworkers, EuroMayDay, San Precario and [Serpica Naro](https://www.serpicanaro.com/serpica-story). She is interested in p2p production, open technologies and etextiles at the intersection between social impact and care practices. She was digital strategist for Arduino (2013-2017) and in 2014 founded WeMake - Milan's Makerspace, where she was involved among others, in projects like [OpenCare]( (http://opencare.cc/)  and [DSI4EU](https://digitalsocial.eu/cluster/1/health-and-care).

###Ivory Tuesday
MSJ is an Anishinaabe Artisan, Knowledge, and Story Holder from Couchiching First Nations located in Treaty Three Territory. Ivory’s Anishinaabe name is Saagegaabowe which means, “Woman who waits by the water” and she is a member of the Lynx Clan. Ivory carries an informed perspective as an Oji-Cree Two-Spirit woman aware of the contextual fluidity of certain Indigenous communities. She currently lives in Thunder Bay, ON and is learning the stories, skills, and ways of life of the more North Eastern Ojibwe Territory of Robinson Superior Treaty, and Treaty #9. Using traditional ecological knowledge and blood memory Ivory reconstructs and redeploys culture and land based skills as it helps contribute to healing, rebuilding, and resurgence. Ivory holds an Honours Bachelor Degree in Social Work, Indigenous Learning and a Masters of Social Justice with a Specialization in Women’s Studies. She was recently hired as a contract lecturer for the Indigenous Learning Department at Lakehead University. Her creative project/thesis revolves around the Indigenous memory technique of the oral tradition used in a performative way as counter surveillance in survival; ‘Moccasin Telegram’. Ivory is most passionate about community engaged work and volunteers with a group named the Wiindo Debwe Mosewin ("walking together in truth"). This group is made up of caring volunteers and friends dedicated to imagining and creating a feminist and decolonized form of community safety together in Thunder Bay. We formed to raise up the voices of the silenced and to respond to the violence in our community, especially violence against women, two spirit people and youth. We stand with those who have been the targets of hate crimes, racism and oppression. Ivory believes healing is in the collective connecting people with Anishinaabe culture and worldview while advancing ‘two-eyed’ seeing through decolonial praxis. ‘Two-eyed seeing’ draws on strengths of both Inidgenous traditions and settler ways. In order to achieve our goals we work with the land, with the ancestors and with care for future generations. We practice contextual fluidity and rely on grassroots methods (like the Mocassin Telegram) for our communications. The climate is changing. We believe hard times are coming and that, for many of us, they are already here. We believe we will suffer alone if we don’t work together.

###Ana Vilenica
is urban and housing activist and freelance urban and cultural researcher.  Among her research interests are the social movements, a mass protests on the periphery of the EU, so-called housing crises, cultural and political action against dominant housing regimes and other urban regeneration schemes, housing of the migrants on the Balkan rout, issues related to social reproduction, new mutations in functioning of contemporary art world as well as potentials for organising with-in and against neoliberal movement from above in brother society. She initiated, realised and collaborated in numerus projects focusing on socio-political, cultural and economic problems like: issues of contemporary migrations, issues of historical revisionism in Serbia, issues related to feminist politics and contemporary motherhood as well as so called housing issue and issues related to art history and contemporary art. She edited the book Becoming a Mother in Neoliberal Capitalism (2013, 2016) and co-edited books On the Ruins of Creative City (2013) and Art and Housing Struggles: Between Art and Political Organising (forthcoming). She is member of the [Radical Housing Journal](https://radicalhousingjournal.org/) collective and the Roof - anti-eviction and housing organisation (Serbia).  


---
tags: syllabus
---
author: pirate.care
---
pub_date: 2019-11-11
