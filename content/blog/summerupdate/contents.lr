title: Pirate Care 2020 Summer Update
---
author:
pirate
---
pub_date: 2020-08-03
---
tags:

syllabus
---
body:

Hello world out there, probably inside somewhere else...

We often read in the opening of emails these days the wish to find a dear one in good health, or well taken care of, in this tough times. Instead, let us open this message by wishing you and your dear ones to be standing in solidarity right now, to be in a state of re-organizing and re-connecting, busy finding concrete ways of being with each other in this tough times, across multiple circles of care.

Here is an overdue update on some of the things that have been going on with us during this last months, full of turmoil for our personal lives as well as for the changes they brought to our practice:

In late 2019 we organized a collective writing retreat hosted by Drugo More (Rijeka, HR) to create a Pirate Care Syllabus - a free digital tool for sharing tacit knowledges and activating political pedagogies from disobedient practices of social reproduction. As [the syllabus and its dedicated library catalog](https://syllabus.pirate.care)(both still a work-in-progress) went online on March 8th, for the opening of WHW's exhibition "...of bread, wine, cars, security and peace" at Kunsthalle Wien, the SARS-CoV-2 pandemic was already bursting in our part of the world, triggering the lockdown of most activities.

So, as the Kunsthalle shut down, we rolled over our work into a collective note-taking effort, joined by a number of amazing new collaborators, to document the unprecedented wave of organising around mutual aid in response to the pandemic. We titled the effort ["Flatten the curve, grow the care: What are we learning from Covid-19"](https://syllabus.pirate.care/topic/coronanotes/), to problematize the famous image depicting a graph of the outbreak, with two curves representing better or worse rates of contagion and an undefined "healthcare capacity" as a straight line that the ideal pandemic curve should not breach. We called it a note-taking effort, and not a syllabus topic, as the pace, novelty and scale of the situation did not allow for our thoughts to sediment just yet and the pedagogy stemming out of the miriad self-organized initiatives will need time to consolidate into practice.

## Cancelled

The lockdown meant that most of the other activities we had planned got halted or cancelled, including:

* The public conversation with Rebecca Gompert, founder of [Women on Waves](https://www.womenonwaves.org/), and activists from the NGO [Sea-Watch](https://sea-watch.org/en), which we had organised at Kunsthalle Wien to launch the syllabus.

* The residency at [Studio Das Weisse Haus](http://www.studiodasweissehaus.at/), scheduled for April, during which we were going to work with [Sezonieri](http://www.sezonieri.at/en/startseite_en/), [QueerBase](https://queerbase.at/), [Cassie Thorton](http://feministeconomicsdepartment.com/), as well as Women on Waves and Sea-Watch, to develop new topics (Check out the latest one on ("Sea-Rescue as Care")[https://syllabus.pirate.care/topic/searescue/] by Sea-Watch) and organize a number of encounters to activate the syllabus.

* The conversations with the network gathered around the critical arts educator space [E.A.R.](http://e-a-r-net.blogspot.com), through which we wanted to deepen our own insight around alternative pedagogies for our times.

* Most crucially, we had to cancel the summer school we were preparing on the island of XX fro September, with the support of [Rijeka European Capital of Culture 2020](https://rijeka2020.eu/en/), where we were going to put the growing syllabus to use and enjoy a collective learning situation with participants and syllabus contributors.

We hope that other chances to activate the syllabus collectively will become possible, as this is one of the core aspects of what we do.

## Online activities

We were grateful to be given the chance to organize some online sessions in the meanwhile. These included Sobramesa, a roundtable conversation with E.A.R. hosted by Nora Landkammer and a small series of online talks and workshops, supported by Kunsthalle Wien and hosted by Andrea Hubin and Flora Schausberger:

* [Pirate Care: Valeria Graziano, Marcell Mars and Tomislav Medak](https://kunsthallewien.at/en/event/pirate-care-ein-talk-mit-valeria-graziano-marcell-mars-and-tomislav-medak/)

* [Mary Maggic: Open Source Estrogen](https://kunsthallewien.at/en/event/mary-maggic-open-source-estrogen/)

* [Sea Watch: Talk with Chris Grodotzki and Morana Miljanović](https://kunsthallewien.at/en/event/sea-watch-talk-with-chris-grodotzki-and-morana-miljanovic/)

* [Cassie Thornton: The Hologram. Collective Health as a “Beautiful Art Work”](https://kunsthallewien.at/en/event/cassie-thornton-the-hologram-collective-health-as-a-beautiful-art-work/)

The videos from the talska nd workshops will be online soon.


## Texts

During this months some of our text were published:

* ["Care and Its Discontents", New Alphabet School, June 7, 2020](https://newalphabetschool.hkw.de/care-and-its-discontents/)

* ["Pirate Care", Artforum, May 11, 2020](https://www.artforum.com/slant/valeria-graziano-marcell-mars-and-tomlsav-medak-on-the-care-crisis-83037)

* ["Against the Crisis", Kunsthalle Wien](https://kunsthallewien.at/en/pirate-care-gegen-die-krise/)


## Talks

We also gave some talks and contributed to some larger conversations about the politics of (re)organizing care in the pandemic:

* [CRIC - Festival of Critical Culture: "Of Fragility, Disposability, Brittleness: Capitalist Abandonment and Care", a talk by Tomislav Medak, June 25, 2020](https://www.facebook.com/watch/live/?v=198771758084988&ref=watch_permalink)

* [RadicalXChange conference: "Caring as an Act of Resistance, a panel with Cassie Thornton, Tomislav Medak and Elsa James, moderated by Marc Garrett, June 19, 2020](https://www.youtube.com/watch?v=eF8Yd0ELqDI)

* [MoneyLab #8 /  Aksioma: "Care: Solidarity and Disobedience", a panel with Cassie Thornton, Tomislav Medak and Maddalena Fragnito, moderated by Davor Mišković, June 1, 2020](https://aksioma.org/moneylab8/)

* [Venice Climate Camp, Webinar “Covid19 e crisi climatica”, with POE (Politics, Ontologies and Ecology); Raul Zibechi; Enrique Leff; Elena Gerebizza (Re:Common); Stefania Barca; Valeria Graziano e Tomislav Medak (Pirate Care); Shell Must Fall; Ende Gelände, May 2, 2020](https://www.youtube.com/playlist?list=PLdvH4tPHvBJ-gY_m9rAacAGOBDJ-8csQc)

* [Podcast with Valeria Graziano & Kitty Worthing (Docs not Cops), Global Staffroom Lunchtime Live Podcast | Manual Labours, April 27, 2020](http://www.manuallabours.co.uk/todo/the-global-staffroom/) [audio](https://soundcloud.com/sophiehope-1/global-staffroom-270420-with-pirate-care-and-docs-not-cops)

* [Discussion by Pirate.Care, with Emina Bužnikić, Iva Marčetić and Ana Vilenica, Versopolis Review - Festival of Hope, April 27, 2020](https://www.versopolis.com/festival-of-hope/festival-of-hope/913/pirate-care-and-the-covid-19-pandemic)

* [Valeria Graziano: Pirate Care, Disruptive Fridays #3, April 17, 2020](https://www.youtube.com/watch?v=stTpVTQFuKQ&feature=youtu.be)

* [Valeria Graziano: Pirate Care, Radio Roža, February, 2020](https://www.mixcloud.com/RadioRo%C5%BEa/prilog-pirate-care/)

## Interviews
We were interviewed:

* [Tomislav Medak: "Neoliberalna bajanja ostat će neduhovita šala", interviewed by Ivana Perić, H-Alter, March 23, 2020](http://www.h-alter.org/vijesti/neoliberalna-bajanja-ostat-ce-neduhovita-sala)

* [Pirate Care: "Taking Care of Unconditional Solidarity", interviewed by Hana Sirovica, Kulturpunkt.hr, March 8, 2020](https://www.kulturpunkt.hr/content/taking-care-unconditional-solidarity)

* [Pirate Care: "Njegovati bezuvjetnu solidarnost", interviewed by Hana Sirovica, Kulturpunkt.hr, March 6, 2020](https://www.kulturpunkt.hr/content/njegovati-bezuvjetnu-solidarnost)

## References
Pirate Care was discussed or referenced:

* [The Radical Housing Jounral Editorial Collective: "Covid-19 and housing struggles: The (re)makings of austerity, disaster capitalism, and the no return to normal"](https://radicalhousingjournal.org/2020/covid-19-and-housing-struggles/)

* [Mercedes Bunz: "Contact Tracing Apps: Should we embrace Surveillance?", blog, April 29, 2020](https://mercedesbunz.net/2020/04/29/630/)

* [La vita oltre la pandemia, Non una di meno, blog, April 28, 2020](https://nonunadimeno.wordpress.com/2020/04/28/la-vita-oltre-la-pandemia/)

* [Josipa Lulić: "Kolektivna skrb: fragmenti za utopiju", Mreža antifašistikinja Zagreba, April 25, 2020](http://www.maz.hr/2020/04/25/kolektivna-skrb-fragmenti-za-utopiju-solidarnosti/)

* [The Care Collective: "COVID-19 pandemic: A Crisis of Care", Versobooks blog, March 26, 2020](https://www.versobooks.com/blogs/4617-covid-19-pandemic-a-crisis-of-care)

* [What, How and for Whom: Kunsthalle Wien's Collective of Artistic Directors, in conversation with Mirela Baciak, Ocula, March 13, 2020](https://ocula.com/magazine/conversations/what-how-for-whom-kunsthalle-wiens-collective/)

* [Hana Sirovica: "Piratski, brižno, neposlušno", Kulturpunkt.hr, January 22, 2020](https://www.kulturpunkt.hr/content/njegovati-bezuvjetnu-solidarnost)


## Things we are doing

* We are re-working the "Flatten The Curve, Grow the Care" notes to hopefully publish them as a stand-alone syllabus, complete with a library of resources as usual.

* We are continuing to work on the Syllabus. Cassie Thorton and Sezonieri are developing new topics and we are looking forward to be able to share their inshgts and tools for organizing later this year.

* We are preparing an exhibition opening in Rijeka in the autumn.
